import express, { Router } from "express";
import bodyParser from "body-parser";
import {
  getAllPosts,
  getPostById,
  editPost,
  createNewPost,
} from "../controllers/newspost";

const router: Router = express.Router();

router.use(bodyParser.json());

/* GET users listing. */
router.get("/", getAllPosts);
router.get("/:id", getPostById);

/* POST users listing. */
router.post("/", createNewPost);

/* PUT users listing. */
router.put("/:id", editPost);

export default router;
