export type Data = {
  id: number;
  title: string;
  text: string;
  createDate: Date;
};

export type Schema = {
  [key: string]: any;
};

export type InputData = {
  title: string;
  text: string;
};
