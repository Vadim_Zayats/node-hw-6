import * as fsPromise from "fs/promises";
import { Random } from "random-js";
import { Data, Schema, InputData } from "../types/types";

interface GetTableOperations {
  getAll: () => Promise<Data[] | undefined>;
  getById: (id: number) => Promise<Data | undefined>;
  create: (dataArray: InputData) => Promise<Data[]>;
  update: (
    id: number,
    newData: Partial<Data>
  ) => Promise<Data | string | undefined>;
  delete: (id: number) => Promise<number | undefined | null>;
}

export class FileDB {
  private schemas: { [key: string]: Schema } = {};
  private static instance: FileDB;

  private constructor() {}

  static getInstance(): FileDB {
    if (!FileDB.instance) {
      FileDB.instance = new FileDB();
    }
    return FileDB.instance;
  }

  private async saveDataToFile(filePath: string, data: Data[]): Promise<void> {
    try {
      await fsPromise.writeFile(filePath, JSON.stringify(data, null, 2));
      console.log(`Дані збережено у файлі ${filePath}`);
    } catch (error) {
      console.error(`Помилка при збереженні даних у файлі ${filePath}:`, error);
    }
  }

  private async readDataFromFile(
    filePath: string
  ): Promise<Data[] | undefined> {
    try {
      const data = await fsPromise.readFile(filePath, "utf-8");
      if (data !== undefined) {
        return JSON.parse(data);
      } else {
        console.log("Помилка: не вдалося прочитати файл.");
      }
    } catch (error) {
      console.error(error);
    }
  }

  async registerSchema(tableName: string, schema: Schema): Promise<void> {
    this.schemas[tableName] = schema;
    const filePath = `${tableName}.json`;
    try {
      await fsPromise.writeFile(filePath, JSON.stringify([], null, 2));
      console.log(`Створено файл бази даних ${tableName}`);
    } catch (error) {
      console.error(
        `Помилка при створенні файлу бази даних ${tableName}:`,
        error
      );
    }
  }

  async saveData(tableName: string, data: Data[]): Promise<void> {
    const filePath = `${tableName}.json`;
    await this.saveDataToFile(filePath, data);
  }

  async readData(tableName: string): Promise<Data[] | undefined> {
    const filePath = `${tableName}.json`;
    const gottedData: Data[] | undefined = await this.readDataFromFile(
      filePath
    );
    const timedData = gottedData?.map(({ createDate, ...other }) => {
      return { ...other, createDate: new Date(createDate) };
    });
    return timedData;
  }

  getTable(tableName: string): GetTableOperations {
    const operations: GetTableOperations = {
      getAll: async (): Promise<Data[] | undefined> => {
        return await this.readData(tableName);
      },
      getById: async (id: number): Promise<Data | undefined> => {
        const allData = await this.readData(tableName);
        if (allData !== undefined) {
          return allData.find((item: Data) => item.id === id);
        } else {
          return undefined;
        }
      },
      create: async (data: InputData): Promise<Data[]> => {
        let db = await this.readData(tableName);
        if (!db) {
          db = [];
        }

        db.push({
          id: new Random().integer(10000, 99999),
          ...data,
          createDate: new Date(),
        });

        await this.saveData(tableName, db);
        return db;
      },
      update: async (
        id: number,
        newData: Partial<InputData>
      ): Promise<Data | string | undefined> => {
        let allData: Data[] | undefined = await this.readData(tableName);
        if (allData !== undefined) {
          const targetData = allData.find((item: Data) => item.id === id);
          if (targetData) {
            const updatedData = {
              ...targetData,
              ...newData,
            };
            // Оновлюємо елемент у масиві
            allData = allData.map((item: Data) =>
              item.id === id ? updatedData : item
            );
            await this.saveData(tableName, allData);
            return updatedData;
          }
          return "Такого посту не існує!";
        }
      },
      delete: async (id: number): Promise<number | undefined | null> => {
        let allData = await this.readData(tableName);
        if (allData !== undefined) {
          const indexToDelete = allData.findIndex(
            (item: Data | undefined) => item && item.id === id
          );
          if (indexToDelete !== -1) {
            const deletedId = allData[indexToDelete].id;
            allData = allData.filter(
              (item: Data | undefined) => item && item.id !== id
            );
            await this.saveData(tableName, allData);
            console.log(`Пост з id "${id}" видалено`);
            return deletedId;
          }
          console.log(`Посту з id "${id}" не існує`);
          return null;
        }
      },
    };

    // Створення нового екземпляру класу GetTableOperations і повернення його
    return new GetTableOperationsInstance(operations);
  }
}

class GetTableOperationsInstance implements GetTableOperations {
  constructor(private operations: GetTableOperations) {}

  getAll(): Promise<Data[] | undefined> {
    return this.operations.getAll();
  }

  getById(id: number): Promise<Data | undefined> {
    return this.operations.getById(id);
  }

  create(dataArray: InputData): Promise<Data[]> {
    return this.operations.create(dataArray);
  }

  update(
    id: number,
    newData: Partial<Data>
  ): Promise<Data | string | undefined> {
    return this.operations.update(id, newData);
  }

  delete(id: number): Promise<number | undefined | null> {
    return this.operations.delete(id);
  }
}

export const fileDB = FileDB.getInstance();

export const newspostTable = fileDB.getTable("newsposts");

export const newspostSchema = {
  id: Number,
  title: String,
  text: String,
  createDate: Date,
};

export const data0: InputData = {
  title: "Title 0",
  text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste, earum accusamus id necessitatibus iusto voluptate expedita cumque minus odit ea autem?",
};
export const data1: InputData = {
  title: "Title 1",
  text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste, earum accusamus id nece.",
};
export const data2: InputData = {
  title: "Title 2",
  text: "Lorem ipsum dolor sit amet consectetur adipisicing iusto voluptate expedita cumque minus odit ea autem cusamus id necessitatibus iusto voluptate expedita cumque minus?",
};
export const data3: InputData = {
  title: "Title 3",
  text: "Lorem ipsum dolor sit amet accusamus id necessitatibus iusto voluptate expedita cumque minus odit ea autem.",
};

(async () => {
  fileDB.registerSchema("newsposts", newspostSchema);
  await newspostTable.create(data0);
  await newspostTable.create(data1);
  await newspostTable.create(data2);
  await newspostTable.create(data3);
})();
