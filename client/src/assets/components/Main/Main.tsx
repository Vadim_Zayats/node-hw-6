import { useState, useEffect } from "react";
import styles from "./main.module.scss";
import { Link } from "react-router-dom";
import { Data } from "../../../../../types/types";
import { Card } from "../Card/Card";

export function Main() {
  const [posts, setPosts] = useState<Data[] | undefined>();

  async function getPosts(): Promise<Data[] | undefined | void> {
    try {
      const res = await fetch(`http://localhost:8000/api/newsposts`);
      if (!res.ok) {
        throw new Error("Server error");
      }
      const data = await res.json();
      return setPosts(data);
    } catch (error) {
      return console.log(error);
    }
  }

  useEffect(() => {
    getPosts();
  }, []);

  return (
    <>
      <Link to={`/newpost`}>
        <button className={`${styles["posts__add-new"]}`}>Додати новину</button>
      </Link>

      <ul className={`${styles["posts__list"]}`}>
        {posts &&
          posts.map((post: Data) => {
            return <Card post={post} />;
          })}
      </ul>
    </>
  );
}
