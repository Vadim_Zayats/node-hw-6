import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import { Data } from "../../../../../types/types";
import { Link } from "react-router-dom";
import styles from "../Main/main.module.scss";
import { URL } from "../../api/url";

export function PostPage() {
  const [post, setPost] = useState<Data | undefined>();

  const { id } = useParams();

  async function getPost(id: number): Promise<void> {
    try {
      const res = await fetch(`${URL}${id}`);
      if (!res.ok) {
        throw new Error("Server error");
      }
      const data = await res.json();
      setPost(data);
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    if (id) {
      getPost(Number(id));
    }
  }, [id]);

  return (
    <>
      <Link to={`/edit-post/${id}`}>
        <button className={`${styles["posts__add-new"]}`}>Змінити пост</button>
      </Link>
      <div>
        {post && (
          <>
            <h3>{post.title}</h3>
            <p>{post.text}</p>
          </>
        )}
      </div>
      <Link to={`/`}>
        <button className={`${styles["posts__add-new"]}`}>
          Назад до усіх новин
        </button>
      </Link>
    </>
  );
}
