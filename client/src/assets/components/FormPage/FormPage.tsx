import styles from "./formPage.module.scss";
import { ChangeEvent, FormEvent, useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { Data, InputData } from "../../../../../types/types";
import { URL } from "../../api/url";

export function FormPage() {
  const { id } = useParams();
  const [post, setPost] = useState<Data | undefined>();
  const [title, setTitle] = useState<string | undefined>("");
  const [text, setText] = useState<string | undefined>("");

  async function getPost(id: number): Promise<void> {
    try {
      const res = await fetch(`${URL}${id}`);
      if (!res.ok) {
        throw new Error("Server error");
      }
      const data = await res.json();
      setPost(data);
    } catch (error) {
      console.log(error);
    }
  }

  async function updatePost(id: number, updPostData: InputData): Promise<void> {
    try {
      const res = await fetch(`${URL}${id}`, {
        method: "PUT",
        body: JSON.stringify(updPostData),
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (!res.ok) {
        throw new Error("Server error");
      }
      const data = await res.json();
      return data;
    } catch (error) {
      console.error(error);
    }
  }

  async function createPost(newPostData: InputData): Promise<void> {
    try {
      const res = await fetch(`${URL}`, {
        method: "POST",
        body: JSON.stringify(newPostData),
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (!res.ok) {
        throw new Error("Server error");
      }
      const data = await res.json();
      return data;
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {
    if (id) {
      getPost(Number(id));
    }
  }, [id]);

  useEffect(() => {
    if (post) {
      setTitle(post.title);
      setText(post.text);
    }
  }, [post]);

  const handleChangeTitle = (e: ChangeEvent<HTMLInputElement>) => {
    setTitle(e.target.value);
  };

  const handleChangeText = (e: ChangeEvent<HTMLInputElement>) => {
    setText(e.target.value);
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    // PUT запит
    if (id && title && text) {
      updatePost(Number(id), { title, text });
      alert("Пост змінено");
    }
    // POST запит
    if (id === undefined && title && text) {
      createPost({ title, text });
      alert("Пост додано");
    }

    // повернутись на "/"
  };

  return (
    <>
      <Link to={`/`}>
        <button className={styles[`form__button`]}>Назад до усіх новин</button>
      </Link>
      <form className={styles[`form__body`]} onSubmit={handleSubmit}>
        <h2>{id ? "Зміна посту" : "Створити новий пост"}</h2>
        <div className={`${styles["form__item"]}`}>
          <label>Заголовок</label>
          <input
            className={styles[`form__title-input`]}
            type="text"
            id="title"
            name="title"
            value={title}
            onChange={handleChangeTitle}
          />
        </div>
        <div className={`${styles["form__item"]}`}>
          <label>Текст</label>
          <input
            className={styles[`form__text-input`]}
            type="text"
            id="text"
            name="text"
            value={text}
            onChange={handleChangeText}
          />
        </div>
        <button className={styles[`form__button`]} type="submit">
          {id ? "Зберегти зміни" : "Створити новину"}
        </button>
      </form>
    </>
  );
}
